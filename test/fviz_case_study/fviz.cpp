#include "fviz.h"

#include <cstdio>

void foo(std::string msg)
{
  printf("From foo(): %s\n", msg.c_str());
}
