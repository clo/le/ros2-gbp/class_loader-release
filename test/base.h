#ifndef __class_loader__test__base__h__
#define __class_loader__test__base__h__

class Base
{
public:
  virtual ~Base() {}
  virtual void saySomething() = 0;
};

#endif  // __class_loader__test__base__h__
